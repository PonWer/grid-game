﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Item;
using Assets.Scripts.Task;
using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts.Tile
{
    public class SoilTile : BaseTile
    {
        public float TimeToGrowFully = 20f;
        public float RemaningTime = 20f;

        public override void Start()
        {
            base.Start();
            RemaningTime = TimeToGrowFully;
            TileManager.GetInstance().TilesThatNeedUpdate.Add(this);
        }

        public override void Mined()
        {
            throw new NotImplementedException();
        }

        public override void SpawnItems()
        {
            throw new NotImplementedException();
        }

        public override TileType GetTileType()
        {
            return TileType.Soil;
        }

        public override void Selected_Mined(bool newState)
        {

        }

        public override Sprite GetSprite()
        {
            return AssetBundle.GetInstance().GetSprite("soil");
        }

        public override void Update()
        {
            base.Update();

            RemaningTime -= Time.deltaTime;
            if (RemaningTime > 0) return;

            var item = Instantiate(AssetBundle.GetInstance().GetPrefab("wheat"));
            item.transform.position = Coordinate;
            item.transform.SetParent(TaskManager.GetInstance().gameObject.transform);
            TaskManager.GetInstance().AddTask(new HaulTask(item.GetComponent<BaseItem>()));
            item.gameObject.transform.parent = GameObject.FindWithTag("ItemRoot").transform;

            RemaningTime = TimeToGrowFully;
        }

        public static Dictionary<ItemType, int> GetBuildCost()
        {
            return new Dictionary<ItemType, int>
            {
                {ItemType.Stone, 1}
            };
        }
    }
}