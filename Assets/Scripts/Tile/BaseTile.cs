﻿using System;
using Assets.Scripts.BuildableObject;
using Assets.Scripts.Task;
using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts.Tile
{
    public abstract class BaseTile : UnityEngine.Tilemaps.Tile
    {
        [Flags]
        public enum TileType
        {
            BaseObject,
            Ground_Dirt,
            Ground_Rock,
            Ground = Ground_Dirt | Ground_Rock,
            Wall,
            Soil
        }

        protected GameObject assignedSprite;
        public BaseObject OccupiedBy = null;

        public bool Modified { get; set; } = false;
        public bool AssignedToATask { get; set; } = false;
        public bool Walkable { get; set; } = true;
        public bool Hidden { get; set; } = true;

        public BaseTask AssignedTask { get; set; }
        public Vector3Int Coordinate { get; set; }


        public int X => Coordinate.x;
        public int Y => Coordinate.y;
        public int Z => Coordinate.z;

        public virtual void Start()
        {
            TileManager.GetInstance().SetColor(Coordinate, Hidden ? HiddenColor() : Color.white);
        }

        public virtual Color NeutralColor()
        {
            return Color.white;
        }
        public virtual Color HiddenColor()
        {
            return Color.black;
        }

        public void ChangeColor(Color color)
        {
            TileManager.GetInstance().SetColor(Coordinate, color);
        }

        public virtual void Mined()
        {
            TileManager.GetInstance().MarkNeigborsAsNotHidden(Coordinate);
        }

        public abstract void SpawnItems();

        public abstract TileType GetTileType();

        public abstract void Selected_Mined(bool newState);

        public abstract Sprite GetSprite();

        protected void ToggleAssignedSprite(bool enable)
        {
            if (enable){
                assignedSprite = Instantiate(AssetBundle.GetInstance().GetPrefab("assigned"), Coordinate,
                    Quaternion.identity);
                assignedSprite.transform.SetParent(GameObject.FindWithTag("TileRoot").transform);
            } else if (assignedSprite != null) {
                assignedSprite.SetActive(false);
                Destroy(assignedSprite);
                assignedSprite = null;
            }
        }

        public virtual void Update()
        {

        }
    }
}