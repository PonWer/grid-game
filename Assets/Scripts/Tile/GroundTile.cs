﻿using System;
using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts.Tile
{
    public class GroundTile : BaseTile
    {
        public TileType GroundType = TileType.Ground_Dirt;




        public override void Mined()
        {
            throw new NotImplementedException();
        }

        public override void SpawnItems()
        {
            throw new NotImplementedException();
        }

        public override TileType GetTileType()
        {
            return TileType.Ground;
        }

        public override void Selected_Mined(bool newState)
        {

        }

        public override Sprite GetSprite()
        {
            return GroundType == TileType.Ground_Rock
                ? AssetBundle.GetInstance().GetSprite("ground_rock")
                : AssetBundle.GetInstance().GetSprite("ground_dirt");
        }
    }
}