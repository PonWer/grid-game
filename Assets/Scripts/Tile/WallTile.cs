﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Item;
using Assets.Scripts.Pathfinding;
using Assets.Scripts.Task;
using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts.Tile
{
    public class WallTile : BaseTile
    {
        public override void Mined()
        {
            base.Mined();

            var newGround = CreateInstance<GroundTile>();
            newGround.GroundType = TileType.Ground_Rock;
            TileManager.GetInstance().ReplaceTile(this, newGround, CustomGrid.TileShared.MovementType.Walkable);

            var item = Instantiate(AssetBundle.GetInstance().GetPrefab("item"));
            item.transform.position = Coordinate;
            item.transform.SetParent(TaskManager.GetInstance().gameObject.transform);
            TaskManager.GetInstance().AddTask(new HaulTask(item.GetComponent<BaseItem>()));
            item.gameObject.transform.parent = GameObject.FindWithTag("ItemRoot").transform;

            ToggleAssignedSprite(false);
        }

        public override void SpawnItems()
        {
            throw new NotImplementedException();
        }

        public override TileType GetTileType()
        {
            return TileType.Wall;
        }

        public override void Selected_Mined(bool newState)
        {
            if (newState == AssignedToATask)
                return;

            if (newState)
            {
                AssignedTask = TaskManager.GetInstance().AddTask(new MineTask(this));
                ToggleAssignedSprite(true);
            }
            else
            {
                if (AssignedTask == null)
                    AssignedTask = TaskManager.GetInstance().FindTask(this, BaseTask.Category.Mine);

                TaskManager.GetInstance().RemoveTask(AssignedTask);
                if (AssignedTask.AssignedUnit != null)
                {
                    AssignedTask.AssignedUnit.currentTask = null;
                    AssignedTask.AssignedUnit = null;
                }

                ToggleAssignedSprite(false);
            }

            AssignedToATask = newState;
        }


        public override void Start()
        {
            Walkable = false;
            base.Start();
        }

        public override Sprite GetSprite()
        {
            return AssetBundle.GetInstance().GetSprite("wall");
        }

        public static Dictionary<ItemType, int> GetBuildCost()
        {
            return new Dictionary<ItemType, int>
            {
                {ItemType.Stone, 2}
            };
        }
    }
}