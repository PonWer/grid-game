﻿using System.Collections.Generic;
using Assets.Scripts.Item;
using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts.BuildableObject
{
    public class Stockpile : BaseObject
    {
        public static Dictionary<ItemType, int> Inventory = new Dictionary<ItemType, int>();
        public ItemType Filter = ItemType.Everything;
        public int Count = 0;
        public int MaxCount = 50;

        public void AddItem(GameObject itemGameObject)
        {
            Count++;
            var item = itemGameObject.GetComponent<BaseItem>();

            if (Inventory.ContainsKey(item.Type()))
                Inventory[item.Type()] += 1;
            else
                Inventory.Add(item.Type(), 1);
        }

        public override bool ObjectPreventMovement => false;

        public void Remove(ItemType item)
        {
            if (Inventory.ContainsKey(item)) Inventory[item] -= 1;
        }

        public GameObject RetriveItem(ItemType itemType)
        {
            Count--;
            GameObject returnItem = null;
            switch (itemType)
            {
                case ItemType.Nothing:
                    break;
                case ItemType.Stone:
                    returnItem = Instantiate(AssetBundle.GetInstance().GetPrefab("item"));
                    Remove(itemType);
                    break;
                case ItemType.Gravel:
                    break;
                case ItemType.Everything:
                    break;
            }

            return returnItem;
        }


        public bool WillAccept(ItemType type)
        {
            return /*Count < MaxCount && */(type & Filter) != 0;
        }


        public bool CanBeRetrived(ItemType itemType)
        {
            return Inventory.ContainsKey(itemType) && Inventory[itemType] > 0;
        }

        public override ObjectType Type()
        {
            return ObjectType.Stockpile;
        }

        public new static Dictionary<ItemType, int> GetBuildCost()
        {
            return new Dictionary<ItemType, int>
            {
                {ItemType.Stone, 0}
            };
        }
    }
}