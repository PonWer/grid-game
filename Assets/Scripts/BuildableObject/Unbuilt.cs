﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Item;
using Assets.Scripts.Pathfinding;
using Assets.Scripts.Task;
using Assets.Scripts.Tile;
using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts.BuildableObject
{
    public class Unbuilt : BaseObject
    {
        private Dictionary<ItemType, int> _buildCost = new Dictionary<ItemType, int>();

        public BaseTile tileBelow;
        private ObjectType _TargetBuildObject;

        public override bool ObjectPreventMovement => true;

        public int RemaningCost
        {
            get
            {
                var count = 0;
                foreach (var buildCostValue in _buildCost.Values) count += buildCostValue;
                return count;
            }
        }

        // Start is called before the first frame update
        private void Start()
        {
            transform.parent = GameObject.FindWithTag("UnbuiltRoot").transform;
        }

        private void AddAllBuildTasks()
        {
            if (RemaningCost == 0)
            {
                TaskManager.GetInstance().AddTask(new BuildTask(this, ItemType.Nothing));
                return;
            }

            foreach (var buildCostValue in _buildCost)
                for (var i = 0; i < buildCostValue.Value; i++)
                    TaskManager.GetInstance().AddTask(new BuildTask(this, buildCostValue.Key));
        }

        public void AddToBuild(ItemType buildItemType)
        {
            if (_buildCost.ContainsKey(buildItemType))
                _buildCost[buildItemType] -= 1;
        }


        public void Created(BaseTile parent, ObjectType toBuildType)
        {
            base.Created(parent);
            this._TargetBuildObject = toBuildType;
            switch (toBuildType)
            {
                case ObjectType.Nothing:
                    break;
                case ObjectType.Stairs:
                    _buildCost = Stairs.GetBuildCost();
                    break;
                case ObjectType.Stockpile:
                    _buildCost = Stockpile.GetBuildCost();
                    break;
                case ObjectType.Unbuilt:
                    break;
                case ObjectType.Wall:
                    _buildCost = WallTile.GetBuildCost();
                    break;
                case ObjectType.Soil:
                    _buildCost = SoilTile.GetBuildCost();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(toBuildType), toBuildType, null);
            }

            AddAllBuildTasks();
        }

        public override void Build()
        {
            switch (_TargetBuildObject)
            {
                case ObjectType.Nothing:
                    throw new NotImplementedException($"Missing implemetation for {_TargetBuildObject}");

                case ObjectType.Stairs:
                    BuildStairs();
                    break;
                case ObjectType.Stockpile:
                    BuildStockpile();
                    break;
                case ObjectType.Unbuilt:
                    throw new NotImplementedException($"Missing implemetation for {_TargetBuildObject}");
                case ObjectType.Wall:
                    BuildWall();
                    break;
                case ObjectType.Soil:
                    BuildSoil();
                    break;
                default:
                    throw new NotImplementedException($"Missing implemetation for {_TargetBuildObject}");
            }


            gameObject.SetActive(false);
            Destroy(gameObject);
        }

        private void BuildWall()
        {
            TileManager.GetInstance().ReplaceTile(
                ParentTile,
                ScriptableObject.CreateInstance<WallTile>(),
                CustomGrid.TileShared.MovementType.NoMovement);
        }

        private void BuildSoil()
        {
            TileManager.GetInstance().ReplaceTile(
                ParentTile,
                ScriptableObject.CreateInstance<SoilTile>(),
                CustomGrid.TileShared.MovementType.SlowMovement);
        }

        private void BuildStockpile()
        {
            var newStockpile = Instantiate(
                AssetBundle.GetInstance().GetPrefab("stockpile"),
                ParentTile.Coordinate, 
                Quaternion.identity).GetComponent<Stockpile>();

            newStockpile.Created(ParentTile);

            StockpileManager.GetInstance().AddStockpile(newStockpile);

            newStockpile.gameObject.transform.parent = GameManager.GetInstance().StockpileRoot;
        }

        private void BuildStairs()
        {
            TileManager.Mining(tileBelow);

            var stairAbove = Instantiate(
                AssetBundle.GetInstance().GetPrefab("stairs"),
                ParentTile.Coordinate,
                Quaternion.identity).GetComponent<Stairs>();


            stairAbove.Created(ParentTile);

            var tileBelowNew = 
                TileManager.GetInstance().GetTileFromVector3(tileBelow.Coordinate);

            var stairsUp = Instantiate(
                AssetBundle.GetInstance().GetPrefab("stairs"),
                tileBelowNew.Coordinate, 
                Quaternion.identity).GetComponent<Stairs>();
            
            stairsUp.Created(tileBelowNew);

            TileManager.GetInstance().AddStairs(stairAbove, Stairs.StairDirection.Down);
            TileManager.GetInstance().AddStairs(stairsUp, Stairs.StairDirection.Up);

            stairAbove.gameObject.transform.parent = GameManager.GetInstance().StairsRoot;
            stairsUp.gameObject.transform.parent = GameManager.GetInstance().StairsRoot;
        }

        public override ObjectType Type()
        {
            return ObjectType.Unbuilt;
        }
    }
}