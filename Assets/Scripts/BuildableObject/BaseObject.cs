﻿using System.Collections.Generic;
using Assets.Scripts.Item;
using Assets.Scripts.Tile;
using UnityEngine;

namespace Assets.Scripts.BuildableObject
{
    public abstract class BaseObject : MonoBehaviour
    {
        public enum ObjectType
        {
            Nothing,
            Stairs,
            Stockpile,
            Unbuilt,
            Wall,
            Soil
        }

        public abstract bool ObjectPreventMovement { get; }

        public BaseTile ParentTile { get; protected set; }
        public Vector3Int Coordinate => ParentTile.Coordinate;
        public abstract ObjectType Type();

        public static Dictionary<ItemType, int> GetBuildCost()
        {
            return new Dictionary<ItemType, int>();
        }

        public virtual void Created(BaseTile newParent)
        {
            ParentTile = newParent;
            ParentTile.OccupiedBy = this;
        }

        public virtual void Removed()
        {
            ParentTile.OccupiedBy = null;
        }

        public virtual void Build()
        {
        }
    }
}