﻿using System.Collections.Generic;
using Assets.Scripts.Item;

namespace Assets.Scripts.BuildableObject
{
    public class Stairs : BaseObject
    {
        public enum StairDirection
        {
            Up = 0,
            Down,
            Both = Up | Down
        }

        public StairDirection Direction;

        public override bool ObjectPreventMovement => false;

        public override ObjectType Type()
        {
            return ObjectType.Stairs;
        }

        public new static Dictionary<ItemType, int> GetBuildCost()
        {
            return new Dictionary<ItemType, int>
            {
                {ItemType.Stone, 2}
            };
        }
    }
}