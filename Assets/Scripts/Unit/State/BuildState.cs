﻿using Assets.Scripts.Item;
using Assets.Scripts.Task;
using UnityEngine;

namespace Assets.Scripts.Unit.State
{
    public class BuildState : BaseState
    {
        private static BuildState _instance;
        public static BuildState Instance => _instance ?? (_instance = new BuildState());

        public override void OnStateEnter(UnitMovement unit)
        {
            if (logEnteringStates)
                Debug.Log("Entering: <color=brown>Building State</color>");

            ((BuildTask) unit.currentTask)?.UpdateTargetStockpile();
        }

        public override void OnStateExit(UnitMovement unit)
        {
            if (logLeavingStates) Debug.Log("Leaving: <color=brown>Building State</color>");
            unit.carry = null;
            unit.currentTask = null;
            unit.ResetPath();
        }

        public override void OnStateUpdate(UnitMovement unit)
        {
            if (unit.currentTask == null)
            {
                unit.ChangeState(IdleState.Instance);
                return;
            }

            if (unit.carry == null && unit.currentTask.TargetItemType != ItemType.Nothing)
                FetchingItem(unit);
            else
                DeliveringItem(unit);
        }

        private static void FetchingItem(UnitMovement unit)
        {
            if (unit.currentTask.TargetStockpile == null)
            {
                ((BuildTask) unit.currentTask).UpdateTargetStockpile();

                if (unit.currentTask.TargetStockpile == null)
                {
                    unit.ChangeState(IdleState.Instance);
                    return;
                }
            }

            unit.SetDestination(unit.currentTask.TargetStockpile.ParentTile.Coordinate);
            if (unit.GetDistanceToDestination() < 1.2f)
            {
                unit.carry =
                    unit.currentTask.TargetStockpile.RetriveItem(unit.currentTask.TargetItemType);
                unit.carry.transform.parent = unit.gameObject.transform;
            }
        }

        private static void DeliveringItem(UnitMovement unit)
        {
            unit.SetDestination(unit.currentTask.TargetUnbuilt.Coordinate);
            if (unit.GetDistanceToDestination() < 0.8f) Build(unit);
        }

        private static void Build(UnitMovement unit)
        {
            if (unit.carry != null)
            {
                unit.carry.transform.parent = null;
                unit.currentTask.TargetUnbuilt.AddToBuild(unit.carry.GetComponent<BaseItem>().Type());
            }

            if (unit.currentTask.TargetUnbuilt.RemaningCost <= 0)
                unit.currentTask.TargetUnbuilt.Build();

            unit.DestroyCarryItem();
            unit.ChangeState(IdleState.Instance);
        }
    }
}