﻿using System;
using Assets.Scripts.Task;
using UnityEngine;

namespace Assets.Scripts.Unit.State
{
    public class IdleState : BaseState
    {
        private static IdleState _instance;
        public static IdleState Instance => _instance ?? (_instance = new IdleState());

        public override void OnStateEnter(UnitMovement unit)
        {
            if (logEnteringStates) Debug.Log("Entering: <color=cyan>Idle State</color>");
            unit.ResetPath();

            TaskManager.GetInstance().RequestTask(unit);
            unit.TaskHasBeenRequested = true;
        }

        public override void OnStateExit(UnitMovement unit)
        {
            if (logLeavingStates) Debug.Log("Leaving: <color=cyan>Idle State</color>");
        }


        public override void OnStateUpdate(UnitMovement unit)
        {
            if (unit.currentTask != null)
            {
                unit.TaskHasBeenRequested = false;
                switch (unit.currentTask.Type)
                {
                    case BaseTask.Category.Build:
                        unit.ChangeState(BuildState.Instance);
                        break;
                    case BaseTask.Category.Mine:
                        unit.ChangeState(MiningState.Instance);
                        break;
                    case BaseTask.Category.Haul:
                        unit.ChangeState(HaulingState.Instance);
                        break;
                    default:
                        throw new Exception($"Missing @currentTask type {unit.currentTask.Type}");
                }
            }
        }
    }
}