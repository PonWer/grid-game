﻿using Assets.Scripts.Task;
using UnityEngine;

namespace Assets.Scripts.Unit.State
{
    public class HaulingState : BaseState
    {
        private static HaulingState _instance;
        public static HaulingState Instance => _instance ?? (_instance = new HaulingState());

        public override void OnStateEnter(UnitMovement unit)
        {
            if (logEnteringStates) Debug.Log("Entering: <color=brown>Hauling State</color>");
            if (unit.currentTask == null) return;

            ((HaulTask) unit.currentTask).UpdateStockpile();
            unit.SetDestination(unit.currentTask.TargetTile.Coordinate);
        }

        public override void OnStateExit(UnitMovement unit)
        {
            if (logLeavingStates) Debug.Log("Leaving: <color=brown>Hauling State</color>");
            unit.carry = null;
            unit.currentTask = null;
            unit.ResetPath();
        }

        public override void OnStateUpdate(UnitMovement unit)
        {
            if (unit.currentTask == null)
            {
                unit.ChangeState(IdleState.Instance);
                return;
            }

            if (unit.carry == null)
                FetchingItem(unit);
            else
                DeliveringItem(unit);
        }

        private static void FetchingItem(UnitMovement unit)
        {
            unit.SetDestination(unit.currentTask.TargetTile.Coordinate);
            if (unit.GetDistanceToDestination() < 1.2f)
            {
                unit.carry = unit.currentTask.TargetGameObject;
                unit.carry.transform.parent = unit.gameObject.transform;
            }
        }

        private static void DeliveringItem(UnitMovement unit)
        {
            unit.SetDestination(unit.currentTask.TargetStockpile.transform.position);
            if (unit.GetDistanceToDestination() < 0.8f)
            {
                unit.carry.transform.parent = null;
                unit.currentTask.TargetStockpile.AddItem(unit.carry);
                unit.DestroyCarryItem();
                unit.ChangeState(IdleState.Instance);
            }
        }
    }
}