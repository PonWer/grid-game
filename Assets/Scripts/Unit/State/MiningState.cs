﻿using Assets.Scripts.Tile;
using UnityEngine;

namespace Assets.Scripts.Unit.State
{
    public class MiningState : BaseState
    {
        private static MiningState _instance;
        public static MiningState Instance => _instance ?? (_instance = new MiningState());

        public override void OnStateEnter(UnitMovement unit)
        {
            if (logEnteringStates) Debug.Log("Entering: <color=blue>Mining State</color>");
            unit.ResetPath();
            if (unit.currentTask == null) return;
            unit.SetDestination(unit.currentTask.TargetTile.Coordinate);
        }

        public override void OnStateExit(UnitMovement unit)
        {
            if (logLeavingStates) Debug.Log("Leaving: <color=blue>Mining State</color>");
            unit.carry = null;
            unit.currentTask = null;
            unit.ResetPath();
        }

        public override void OnStateUpdate(UnitMovement unit)
        {
            if (unit.currentTask == null)
            {
                unit.ChangeState(IdleState.Instance);
                return;
            }

            if (unit.GetDistanceToDestination() < unit.mineDistance)
            {
                TileManager.Mining(unit.currentTask.TargetTile);
                unit.ChangeState(IdleState.Instance);
            }
        }
    }
}