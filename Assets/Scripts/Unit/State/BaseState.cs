﻿namespace Assets.Scripts.Unit.State
{
    public abstract class BaseState
    {
        public bool logEnteringStates = false;
        public bool logLeavingStates = false;

        public abstract void OnStateEnter(UnitMovement unit);

        public abstract void OnStateExit(UnitMovement unit);

        public abstract void OnStateUpdate(UnitMovement unit);
    }
}