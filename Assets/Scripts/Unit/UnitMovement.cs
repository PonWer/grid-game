﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Pathfinding;
using Assets.Scripts.Task;
using Assets.Scripts.Tile;
using Assets.Scripts.Unit.State;
using UnityEngine;

namespace Assets.Scripts.Unit
{
    public class UnitMovement : MonoBehaviour
    {
        public BaseState CurrentState { get; private set; } = IdleState.Instance;

        private void Awake()
        {
            lr = GetComponent<LineRenderer>();
        }

        private void Start()
        {
            CurrentState.OnStateEnter(this);
            UnitManager.GetInstance().Units.Add(this);
        }

        public void _Update()
        {
            CurrentState.OnStateUpdate(this);
            Move();
        }

        public void ChangeState(BaseState nextState)
        {
            if (CurrentState == nextState)
                return;

            CurrentState.OnStateExit(this);
            CurrentState = nextState;
            CurrentState.OnStateEnter(this);
        }

        public void DestroyCarryItem()
        {
            if (carry == null)
                return;

            carry.gameObject.SetActive(false);
            Destroy(carry);
            carry = null;
        }

        #region pathfinding

        private Vector3 _destination;
        public List<BaseTile> Path { get; private set; } = new List<BaseTile>();
        private bool _isSearching;
        public float movingSpeed = 0.5f;
        public float moveToNextTileDistance = 0.6f;
        private LineRenderer lr;

        private void Move()
        {
            if (Path.Count <= 0) return;

            foreach (var v in Path)
                if (v.Modified)
                {
                    //Debug.LogWarning("A pathTile was removed and path reset");
                    ResetPath();
                    return;
                }

            if (Math.Abs(transform.position.z - Path[0].Coordinate.z) > 0.1f)
                transform.position = new Vector3(transform.position.x, transform.position.y, Path[0].Coordinate.z);

            var step = movingSpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, Path[0].Coordinate, step);

            if (Path.Count != 1 && Vector3.Distance(transform.position, Path[0].Coordinate) < moveToNextTileDistance)
            {
                Path.RemoveAt(0);
                PaintPath();
            }
        }

        public void PaintPath()
        {
            lr.positionCount = Path.Count;
            lr.SetPositions(Path.ConvertAll(x => (Vector3) x.Coordinate + new Vector3(0, 0, -0.1f)).ToArray());
        }

        public void ResetPath()
        {
            _destination = transform.position;
            Path.Clear();
            if (_isSearching) OnGoingJob.Abort = true;

            PaintPath();
        }

        public void SetDestination(Vector3 destination)
        {
            if (destination == _destination /*&& destination == Path[Path.Count - 1]*/)
                return;

            if (_isSearching) OnGoingJob.Abort = true;


            _isSearching = true;

            _destination = destination;

            Path.Clear();

            OnGoingJob = JobManager.GetInstance().RequestPath(
                TileManager.GetInstance().GetClosestTileFromVector3(transform.position),
                TileManager.GetInstance().GetClosestTileFromVector3(_destination),
                PathFindingComplete);
        }

        public float GetDistanceTo(Vector3 pos)
        {
            return Vector3.Distance(gameObject.transform.position, pos);
        }

        public float GetDistanceToDestination()
        {
            var distance = Vector3.Distance(gameObject.transform.position, _destination);
            if (Math.Abs(gameObject.transform.position.z - _destination.z) > 0.2f)
                distance *= 5.0f; //TaskOnAotherLayerWeightCost;
            return distance;
        }

        public void PathFindingComplete(Pathfinder jobDoneObject)
        {
            lock (jobDoneObject)
            {
                _isSearching = false;
                if (jobDoneObject.Abort)
                    return;

                if (jobDoneObject.FoundPath.Count == 0)
                {
                    ReturnTask();
                    return;
                }


                Path = (from tile in jobDoneObject.FoundPath
                    select TileManager.GetInstance().GetTileFromVector3(tile)).ToList();
            }

            PaintPath();
        }

        #endregion

        #region taskRelated

        public float mineDistance = 1.2f;
        public GameObject carry;
        public BaseTask currentTask;
        protected internal Pathfinder OnGoingJob;
        internal bool TaskHasBeenRequested;

        public void AssignTask(BaseTask task)
        {
            currentTask = task;
            task.AssignedUnit = this;
        }

        public void ReturnTask()
        {
            TaskManager.GetInstance().AddTask(currentTask);
            currentTask.AssignedUnit = null;
            currentTask = null;
        }

        #endregion
    }
}