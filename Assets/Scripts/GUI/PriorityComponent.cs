﻿using System;
using Assets.Scripts.Task;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.GUI
{
    public class PriorityComponent : MonoBehaviour
    {
        public BaseTask.Category Category;

        public Toggle Enabled;

        public Slider PrioSlider;

        public Text PrioText;

        public Text Text;

        public void PrioChanged(float newValue)
        {
            TaskManager.GetInstance().ChangePrio(Category, (int) newValue);
        }


        public void Toggled(bool newState)
        {
            if (newState)
                TaskManager.GetInstance().StopIgnoringTaskCategory(Category);
            else
                TaskManager.GetInstance().BeginIgnoringTaskCategory(Category);
        }


        // Start is called before the first frame update
        private void Start()
        {
            Enabled.onValueChanged.AddListener(Toggled);
            PrioSlider.onValueChanged.AddListener(PrioChanged);
            UpdatePriority(null, null);
            TaskManager.GetInstance().OnPriorityChanged += UpdatePriority;
        }


        public void UpdatePriority(object sender, EventArgs e)
        {
            PrioSlider.value = TaskManager.GetInstance().GetPriorityValue(Category);
            PrioText.text = PrioSlider.value.ToString();
        }
    }
}