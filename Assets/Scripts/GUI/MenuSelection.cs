﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.GUI
{
    public class MenuSelection : MonoBehaviour
    {
        private readonly Color _neutralColor = Color.white;
        private Button _prevSelectedButton;
        private readonly Color _selectedColor = Color.green;

        public GameObject PriorityScreen;

        public void TogglePriorityScreen()
        {
            PriorityScreen.SetActive(!PriorityScreen.activeSelf);
        }


        public void Select(Button button)
        {
            if (_prevSelectedButton)
                _prevSelectedButton.GetComponent<Image>().color = _neutralColor;

            var hideBuildScreen = true;
            switch (button.name)
            {
                case "MineButton":
                    button.GetComponent<Image>().color = _selectedColor;
                    GameManager.GetInstance().SetState(GameManager.State.Mine);
                    break;
                case "PriorityButton":
                    button.GetComponent<Image>().color = _selectedColor;
                    GameManager.GetInstance().SetState(GameManager.State.Default);
                    break;
                case "BuildButton":
                    button.GetComponent<Image>().color = _selectedColor;
                    gameObject.GetComponentInChildren<BuildButton>().Pressed();
                    hideBuildScreen = false;
                    break;
                case "StockpileButton":
                    GameManager.GetInstance().SetState(GameManager.State.Stockpile);
                    break;
                case "StairsButton":
                    GameManager.GetInstance().SetState(GameManager.State.Stairs);
                    break;
                case "WallButton":
                    GameManager.GetInstance().SetState(GameManager.State.Wall);
                    break;
                case "SoilButton":
                    GameManager.GetInstance().SetState(GameManager.State.Soil);
                    break;
                default: //select
                    Debug.LogWarning($"Missing button handling {button.name}");
                    _prevSelectedButton = null;
                    GameManager.GetInstance().SetState(GameManager.State.Default);
                    break;
            }

            if (hideBuildScreen)
                GetComponentInChildren<BuildButton>().Hide();

            _prevSelectedButton = button;
        }
    }
}