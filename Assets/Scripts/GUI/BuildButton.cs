﻿using UnityEngine;

namespace Assets.Scripts.GUI
{
    public class BuildButton : MonoBehaviour
    {
        public CanvasRenderer dropDown;

        public bool DropDownIsActive;

        // Use this for initialization
        private void Start()
        {
            dropDown.gameObject.SetActive(false);
            DropDownIsActive = false;
        }

        public void Pressed()
        {
            DropDownIsActive = !DropDownIsActive;
            dropDown.gameObject.SetActive(DropDownIsActive);
        }

        public void Hide()
        {
            DropDownIsActive = false;
            dropDown.gameObject.SetActive(DropDownIsActive);
        }
    }
}