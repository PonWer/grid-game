﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Tile;
using UnityEngine;

namespace Assets.Scripts.Pathfinding
{
    public class Pathfinder
    {
        private readonly List<CustomGrid.PathTile> _openSet = new List<CustomGrid.PathTile>();
        private readonly HashSet<CustomGrid.PathTile> _closedSet = new HashSet<CustomGrid.PathTile>();
        private readonly JobManager.JobComplete _completeCallback;
        private readonly Dictionary<Vector3Int, CustomGrid.PathTile> _tiles = new Dictionary<Vector3Int, CustomGrid.PathTile>();
        protected internal CustomGrid.PathTile CurrentPathTile;
        
        public List<Vector3Int> FoundPath = new List<Vector3Int>();
        public volatile bool JobDone;
        public volatile bool Abort = false;

        private readonly Vector3Int _startCoordinate;
        private readonly Vector3Int _endCoordinate;
        private readonly int _weight;

        //Constructor
        public Pathfinder(Vector3Int startPos, Vector3Int endPos, JobManager.JobComplete callback, int weigh)
        {
            _weight = weigh;
            _completeCallback = callback;
            _startCoordinate = startPos;
            _endCoordinate = endPos;
        }

        public void NotifyComplete() => _completeCallback?.Invoke(this);

        public void Execute()
        {
            _openSet.Add(GetNewTile(_startCoordinate));
            while (_openSet.Count > 0)
            {
                if (Abort)
                {
                    JobDone = true;
                    return;
                }

                CurrentPathTile = _openSet[0];
                foreach (var inOpenset in _openSet)
                    //We check if there is a better pathTile
                    if (inOpenset.fCost < CurrentPathTile.fCost ||
                        inOpenset.fCost == CurrentPathTile.fCost && inOpenset.hCost < CurrentPathTile.hCost)
                        CurrentPathTile = inOpenset;

                //we remove the current Tile from the open set and add to the closed set
                _openSet.Remove(CurrentPathTile);
                _closedSet.Add(CurrentPathTile);

                if (CurrentPathTile.Coordinate == _endCoordinate)
                {
                    FoundPath = RetracePath(GetNewTile(_startCoordinate), CurrentPathTile);
                    break;
                }

                //if we haven't reached our targetTile, then we need to start looking the neighbors
                AddNeighbors(CurrentPathTile);
            }

            JobDone = true;
        }

        private int DiagonalHeuristic(Vector3Int posA, Vector3Int posB)
        {
            var dx = Math.Abs(posA.x - posB.x);
            var dy = Math.Abs(posA.y - posB.y);

            // D2 diagonal cost
            // D = linear cost
            //return D * max(dx, dy) + (D2-D) * min(dx, dy)
            return 10 * Math.Max(dx, dy) + 4 * Math.Min(dx, dy);
        }

        private List<Vector3Int> RetracePath(CustomGrid.PathTile startPathTile, CustomGrid.PathTile endPathTile)
        {
            var path = new List<Vector3Int>();
            var currentTile = endPathTile;

            while (currentTile != startPathTile)
            {
                path.Add(currentTile.Coordinate);
                currentTile = currentTile.Parent;
            }

            path.Reverse();
            return path;
        }

        private void AddNeighbors(CustomGrid.PathTile pathTile)
        {
            //g & h should use the same calculation
            //however DiagonalHeuristic will return these precomputed values
            var LinearCost = 10;
            var DiagonalCost = 14;

            //linear
            var west = GetNeighborTile(pathTile.Coordinate + new Vector3Int(-1, 0, 0));
            var east = GetNeighborTile(pathTile.Coordinate + new Vector3Int(1, 0, 0));
            var south = GetNeighborTile(pathTile.Coordinate + new Vector3Int(0, -1, 0));
            var north = GetNeighborTile(pathTile.Coordinate + new Vector3Int(0, 1, 0));
            //diagonal
            var northEast = north != null && east != null
                ? GetNeighborTile(pathTile.Coordinate + new Vector3Int(1, 1, 0))
                : null;
            var northWest = north != null && west != null
                ? GetNeighborTile(pathTile.Coordinate + new Vector3Int(-1, 1, 0))
                : null;
            var southEast = south != null && east != null
                ? GetNeighborTile(pathTile.Coordinate + new Vector3Int(1, -1, 0))
                : null;
            var southWest = south != null && west != null
                ? GetNeighborTile(pathTile.Coordinate + new Vector3Int(-1, -1, 0))
                : null;
            //3D
            var below = pathTile.IsShaftDown() ? GetNeighborTile(pathTile.Coordinate + new Vector3Int(0, 0, 1)) : null;
            var above = pathTile.IsShaftUp() ? GetNeighborTile(pathTile.Coordinate + new Vector3Int(0, 0, -1)) : null;

            //Adding them to openlist
            if (west != null) TestNeighbor(west, LinearCost);
            if (east != null) TestNeighbor(east, LinearCost);
            if (south != null) TestNeighbor(south, LinearCost);
            if (north != null) TestNeighbor(north, LinearCost);
            if (northEast != null) TestNeighbor(northEast, DiagonalCost);
            if (northWest != null) TestNeighbor(northWest, DiagonalCost);
            if (southEast != null) TestNeighbor(southEast, DiagonalCost);
            if (southWest != null) TestNeighbor(southWest, DiagonalCost);
            if (below != null) TestNeighbor(below, LinearCost);
            if (above != null) TestNeighbor(above, LinearCost);
        }

        private void TestNeighbor(CustomGrid.PathTile neighbor, int gCost)
        {
            if (_closedSet.Contains(neighbor)) return;

            //we create a new movement cost for our neighbors
            var gCostNew = CurrentPathTile.gCost + gCost;
            var isInOpenSet = _openSet.Contains(neighbor);

            if (gCostNew < neighbor.gCost || !isInOpenSet)
            {
                neighbor.gCost = gCostNew;
                neighbor.hCost = _weight * DiagonalHeuristic(neighbor.Coordinate, _endCoordinate);
                neighbor.fCost = neighbor.gCost + neighbor.hCost;
                neighbor.Parent = CurrentPathTile;

                if (!isInOpenSet) _openSet.Add(neighbor);
            }
        }

        private CustomGrid.PathTile GetNeighborTile(Vector3Int pos)
        {
            var tile = GetNewTile(pos);

            return tile != null &&
                   (tile.IsWalkable() || tile.Coordinate == _endCoordinate)
                ? tile
                : null;
        }


        public CustomGrid.PathTile GetNewTile(Vector3Int pos)
        {
            if (_tiles.ContainsKey(pos)) return _tiles[pos];
            //else

            var newTileData = TileManager.GetInstance().CustomGrid.GetPathTile(pos);
            _tiles.Add(pos, newTileData);
            return newTileData;
        }
    }
}