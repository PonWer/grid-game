﻿using System.Collections.Generic;
using System.Threading;
using Assets.Scripts.Tile;
using UnityEngine;

namespace Assets.Scripts.Pathfinding
{
    //This class controls the threads
    public class JobManager : MonoBehaviour
    {
        //Delegates are a variable that points to a function
        public delegate void JobComplete(Pathfinder jobObject);

        //Singleton
        private static JobManager _instance;

        private readonly List<Pathfinder> _currentJobs = new List<Pathfinder>();
        private readonly List<Pathfinder> _todoJobs = new List<Pathfinder>();

        public int AStarWeight = 1;

        //The maximum simultaneous threads we allow to open
        public int maxJobs = 3;

        private void Awake()
        {
            _instance = this;
        }

        public static JobManager GetInstance()
        {
            return _instance;
        }


        private void LateUpdate()
        {
            /*
             * Another way to keep track of the threads we have open would have been to create 
             * a new thread for it but we can also just use Unity's main thread too since this class
             * derives from monoBehaviour
             */

            var i = 0;

            while (i < _currentJobs.Count)
                if (_currentJobs[i].JobDone)
                {
                    _currentJobs[i].NotifyComplete();
                    _currentJobs.RemoveAt(i);
                }
                else
                {
                    i++;
                }

            if (_todoJobs.Count > 0 && _currentJobs.Count < maxJobs)
            {
                var job = _todoJobs[0];
                _todoJobs.RemoveAt(0);
                _currentJobs.Add(job);

                //Start a new thread
                var jobThread = new Thread(job.Execute);
                jobThread.Start();
            }
        }

        public Pathfinder RequestPath(BaseTile startTile, BaseTile targetTile, JobComplete CompleteCallback)
        {
            if (startTile == null)
            {
                Debug.LogError("Pathfinder start is null");
                return null;
            }

            if (targetTile == null)
            {
                Debug.LogError("Pathfinder targetTile is null");
                return null;
            }

            var newJob = new Pathfinder(startTile.Coordinate, targetTile.Coordinate, CompleteCallback, AStarWeight);
            _todoJobs.Add(newJob);
            return newJob;
        }
    }
}