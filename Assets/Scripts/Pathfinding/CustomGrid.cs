﻿using System;
using Assets.Scripts.Tile;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Assets.Scripts.Pathfinding
{
    public class CustomGrid
    {
        private readonly TileShared[,,] _grid;

        public int maxX;
        public int maxY;
        public int maxZ;

        public CustomGrid(int x, int y, int z, ref Tilemap tileMap)
        {
            maxX = x;
            maxY = y;
            maxZ = z;
            _grid = new TileShared[maxX, maxY, maxZ];


            for (var i = 0; i < maxX; i++)
            for (var j = 0; j < maxY; j++)
            for (var k = 0; k < maxZ; k++)
            {
                var pos = new Vector3Int(i, j, k);
                _grid[i, j, k] = new TileShared(pos);

                if (tileMap.GetTile<BaseTile>(pos).Walkable)
                    _grid[i, j, k].MoveCost = TileShared.MovementType.NormalMovement;
            }
        }

        public PathTile GetPathTile(Vector3Int pos)
        {
            return
                pos.x < maxX && pos.x >= 0 &&
                pos.y < maxY && pos.y >= 0 &&
                pos.z < maxZ && pos.z >= 0
                    ? new PathTile(_grid[pos.x, pos.y, pos.z])
                    : null;
        }

        public void ChangeTile(Vector3Int pos, TileShared.MovementType cost) => _grid[pos.x, pos.y, pos.z].MoveCost = cost;

        public class TileShared
        {
            [Flags]
            public enum MovementType
            {
                NoMovement = 0x0000,
                NormalMovement = 0x0001,
                ShaftUp = 0x0002,
                ShaftDown = 0x0004,
                FastMovement = 0x0008,
                SlowMovement = 0x0016,
                Walkable = NormalMovement | FastMovement | ShaftUp | ShaftDown
            }

            public readonly Vector3Int Coordinate;
            public MovementType MoveCost = MovementType.NoMovement;

            public TileShared(Vector3Int inPos)
            {
                Coordinate = inPos;
            }
        }

        public class PathTile
        {
            public int fCost;
            public int gCost;
            public int hCost;

            private readonly TileShared gridTile;
            public PathTile Parent;

            public PathTile(TileShared root)
            {
                gridTile = root;
            }

            public Vector3Int Coordinate => gridTile.Coordinate;
            public bool IsWalkable() => (TileShared.MovementType.Walkable & gridTile.MoveCost) != 0;
            public bool IsShaftUp() => gridTile.MoveCost == TileShared.MovementType.ShaftUp;
            public bool IsShaftDown() => gridTile.MoveCost == TileShared.MovementType.ShaftDown;
        }
    }
}