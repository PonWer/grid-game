﻿using System;
using System.Collections.Generic;
using Assets.Scripts.BuildableObject;
using Assets.Scripts.Pathfinding;
using Assets.Scripts.Utility;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Assets.Scripts.Tile
{
    //[ExecuteInEditMode]
    public class TileManager : MonoBehaviour
    {
        //Singleton
        private static TileManager _instance;
        private Tilemap _grid;
        public CustomGrid CustomGrid;

        //Setting up the grid
        public int maxX = 20;
        public int maxY = 1;
        public int maxZ = 20;

        private readonly List<Stairs> _stairs = new List<Stairs>();

        public List<BaseTile> TilesThatNeedUpdate = new List<BaseTile>();

        // Use this for initialization
        private void Awake()
        {
            _instance = this;
            _grid = GetComponentInChildren<Tilemap>();
        }

        private void Start()
        {
            StaticSpawnMethod();
            CustomGrid = new CustomGrid(maxX, maxY, maxZ, ref _grid);
        }

        private void Update()
        {
            foreach (var baseTile in TilesThatNeedUpdate)
            {
                baseTile.Update();
            }
        }

        public static TileManager GetInstance()
        {
            return _instance;
        }


        private void StaticSpawnMethod()
        {
            _grid.ClearAllTiles();

            for (var x = 0; x < maxX; x++)
            for (var y = 0; y < maxY; y++)
            for (var z = 0; z < maxZ; z++)
            {
                var isWall = z != 0 || x > maxX / 8;


                var refTile = isWall
                    ? ScriptableObject.CreateInstance<WallTile>()
                    : ScriptableObject.CreateInstance<GroundTile>() as BaseTile;

                var position = new Vector3Int(x, y, z);

                refTile.Coordinate = position;

                refTile.sprite = refTile.GetSprite();

                refTile.Hidden = true;

                _grid.SetTile(position, refTile);
                _grid.SetTileFlags(position, TileFlags.None);

                refTile.Start();
            }

            UnhideAllReachableTiles();
        }

        private void UnhideAllReachableTiles()
        {
            for (var x = 0; x < maxX; x++)
            for (var y = 0; y < maxY; y++)
            for (var z = 0; z < maxZ; z++)
            {
                var pos = new Vector3Int(x, y, z);
                if (IsTileReachable(pos))
                    UnHideTile(GetTileFromVector3(pos));
            }
        }

        public Unbuilt BuildUnbuilt(BaseObject.ObjectType type, BaseTile targeTile)
        {
            var unbuilt =
                Instantiate(AssetBundle.GetInstance().GetPrefab("unbuilt"), targeTile.Coordinate, Quaternion.identity)
                    .GetComponent<Unbuilt>();
            unbuilt.Created(targeTile, type);
            return unbuilt;
        }

        public void MarkNeigborsAsNotHidden(Vector3Int coordinate)
        {
            //
            UnHideTile(GetTile(coordinate.x - 1, coordinate.y, coordinate.z));
            UnHideTile(GetTile(coordinate.x + 1, coordinate.y, coordinate.z));
            UnHideTile(GetTile(coordinate.x, coordinate.y - 1, coordinate.z));
            UnHideTile(GetTile(coordinate.x, coordinate.y + 1, coordinate.z));

            //diagonal
            UnHideTile(GetTile(coordinate.x - 1, coordinate.y - 1, coordinate.z));
            UnHideTile(GetTile(coordinate.x - 1, coordinate.y + 1, coordinate.z));
            UnHideTile(GetTile(coordinate.x + 1, coordinate.y - 1, coordinate.z));
            UnHideTile(GetTile(coordinate.x + 1, coordinate.y + 1, coordinate.z));
        }

        private void UnHideTile(BaseTile tile)
        {
            if (tile == null || !tile.Hidden)
                return;
            tile.Hidden = false;
            SetColor(tile.Coordinate, tile.NeutralColor());
        }

        public bool IsTileReachable(Vector3Int coordinate)
        {
            var tile1 = GetTile(coordinate.x - 1, coordinate.y, coordinate.z);
            var tile2 = GetTile(coordinate.x + 1, coordinate.y, coordinate.z);
            var tile3 = GetTile(coordinate.x, coordinate.y - 1, coordinate.z);
            var tile4 = GetTile(coordinate.x, coordinate.y + 1, coordinate.z);

            return tile1 && tile1.Walkable ||
                   tile2 && tile2.Walkable ||
                   tile3 && tile3.Walkable ||
                   tile4 && tile4.Walkable;
        }

        public BaseTile GetTile(int x, int y, int z)
        {
            BaseTile retVal = null;

            if (x < maxX && x >= 0 &&
                y >= 0 && y < maxY &&
                z >= 0 && z < maxZ)
                retVal = _grid.GetTile(new Vector3Int(x, y, z)) as BaseTile;

            return retVal;
        }

        public BaseTile GetTile(Vector3Int pos)
        {
            return GetTile(pos.x, pos.y, pos.z);
        }

        public void MovePosToWithingGrid(ref int x, ref int y, ref int z)
        {
            if (x >= maxX) x = maxX - 1;

            if (x < 0) x = 0;

            if (y >= maxY) y = maxY - 1;

            if (y < 0) y = 0;

            if (z >= maxZ) z = maxZ - 1;

            if (z < 0) z = 0;
        }

        public BaseTile GetTileFromVector3(Vector3 pos)
        {
            var x = Mathf.RoundToInt(pos.x);
            var y = Mathf.RoundToInt(pos.y);
            var z = Mathf.RoundToInt(pos.z);

            //MovePosToWithingGrid(ref x, ref y, ref z);

            var retVal = GetTile(x, y, z);
            return retVal;
        }

        public BaseTile GetTileFromVector3(Vector3Int coordinate)
        {
            var retVal = GetTile(coordinate.x, coordinate.y, coordinate.z);
            return retVal;
        }

        public BaseTile GetClosestTileFromVector3(Vector3 pos)
        {
            var x = Mathf.RoundToInt(pos.x);
            var y = Mathf.RoundToInt(pos.y);
            var z = Mathf.RoundToInt(pos.z);

            MovePosToWithingGrid(ref x, ref y, ref z);

            var retVal = GetTile(x, y, z);
            return retVal;
        }

        public void ReplaceTile(BaseTile oldTile, BaseTile newTile, CustomGrid.TileShared.MovementType newMode)
        {
            oldTile.Modified = true;
            newTile.Coordinate = oldTile.Coordinate;
            newTile.name = oldTile.name;
            newTile.sprite = newTile.GetSprite();

            _grid.SetTile(oldTile.Coordinate, newTile);
            CustomGrid.ChangeTile(oldTile.Coordinate, newMode);

            newTile.Start();
            //delete oldTile;
        }

        public static void Mining(BaseTile taskTarget)
        {
            switch (taskTarget.GetTileType())
            {
                case BaseTile.TileType.BaseObject:
                    throw new InvalidCastException();
                case BaseTile.TileType.Ground:
                    break;
                case BaseTile.TileType.Wall:
                    taskTarget.Mined();
                    break;
                default:
                    throw new InvalidCastException();
            }
        }

        public void SetColor(Vector3Int coordinate, Color color)
        {
            _grid.SetColor(coordinate, color);
        }

        public void AddStairs(Stairs stairs, Stairs.StairDirection dir)
        {
            stairs.Direction = dir;
            this._stairs.Add(stairs);
            CustomGrid.ChangeTile(stairs.Coordinate,
                dir == Stairs.StairDirection.Down
                    ? CustomGrid.TileShared.MovementType.ShaftDown
                    : CustomGrid.TileShared.MovementType.ShaftUp);
        }
    }
}