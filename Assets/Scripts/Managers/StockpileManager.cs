﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Item;
using UnityEngine;

namespace Assets.Scripts.BuildableObject
{
    public class StockpileManager : MonoBehaviour
    {
        //Singleton
        private static StockpileManager _instance;
        public List<Stockpile> Stockpiles { get; } = new List<Stockpile>();
        public event EventHandler OnStockpileChanges;

        // Use this for initialization
        private void Start()
        {
            //OnStockpileChanges += NotifyAllHaulTasks;
        }

        // Update is called once per frame
        private void Update()
        {
        }

        public void AddStockpile(Stockpile newStockpile)
        {
            Stockpiles.Add(newStockpile);
            OnStockpileChanges?.Invoke(this, null);
        }

        public void RemoveStockpile(Stockpile oldStockpile)
        {
            Stockpiles.Remove(oldStockpile);
            OnStockpileChanges?.Invoke(this, null);
        }

        public void UpdatedStockpile(Stockpile oldStockpile)
        {
            OnStockpileChanges?.Invoke(this, null);
        }

        private void Awake()
        {
            _instance = this;
        }

        public static StockpileManager GetInstance()
        {
            return _instance;
        }

        public Stockpile GetClosestStockpile(Vector3 pos, ItemType itemType, bool input)
        {
            var result = from stockpile in Stockpiles
                where input ? stockpile.WillAccept(itemType) : stockpile.CanBeRetrived(itemType)
                orderby Vector3.Distance(stockpile.transform.position, pos)
                select stockpile;

            return result.Any() ? result.ElementAt(0) : null;
        }

        public bool CheckIfStockpileExist(ItemType itemType, bool input)
        {
            var test = from stockpile in Stockpiles
                where input ? stockpile.WillAccept(itemType) : stockpile.CanBeRetrived(itemType)
                select stockpile;

            return test.Any();
        }
    }
}