﻿using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts
{
    public class GameManager : MonoBehaviour
    {
        public Transform StockpileRoot;
        public Transform StairsRoot;
        public Transform ItemsRoot;
        public Transform UnbuiltRoot;

        public enum State
        {
            Default,
            Mine,
            Move,
            Stockpile,
            Stairs,
            Wall,
            Soil
        }

        //Singletons
        private static GameManager _instance;
        public GameObject _gui;

        private State _state;
        public int CurrentWorldLayer = 0;

        public State GetState() 
        {
            return _state;
        }

        public void SetState(State newState)
        {
            _state = newState;
        }

        // Use this for initialization
        private void Start()
        {
            _gui.name = "GUI";
        }

        private void Awake()
        {
            _instance = this;
            _state = State.Default;

            StockpileRoot = GameObject.FindWithTag("StockpileRoot").transform;
            StairsRoot = GameObject.FindWithTag("StairsRoot").transform;
            ItemsRoot = GameObject.FindWithTag("ItemRoot").transform;
            UnbuiltRoot = GameObject.FindWithTag("UnbuiltRoot").transform;
        }

        public static GameManager GetInstance()
        {
            return _instance;
        }
    }
}