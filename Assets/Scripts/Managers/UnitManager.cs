﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Unit
{
    public class UnitManager : MonoBehaviour
    {
        private static UnitManager _instance;
        public List<UnitMovement> Units = new List<UnitMovement>();

        private void Awake()
        {
            _instance = this;
        }

        public static UnitManager GetInstance()
        {
            return _instance;
        }

        // Update is called once per frame
        private void Update()
        {
            for (var i = 0; i < Units.Count; i++) Units[i]._Update();
        }
    }
}