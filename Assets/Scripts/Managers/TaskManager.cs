﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Tile;
using Assets.Scripts.Unit;
using UnityEngine;

namespace Assets.Scripts.Task
{
    public class TaskManager : MonoBehaviour
    {
        private readonly Dictionary<int, BaseTask.Category> _priority = new Dictionary<int, BaseTask.Category>();
        private readonly Dictionary<BaseTask.Category, bool> _TaskEnabled = new Dictionary<BaseTask.Category, bool>();

        private readonly Dictionary<BaseTask.Category, List<BaseTask>> _tasksByType =
            new Dictionary<BaseTask.Category, List<BaseTask>>();

        private readonly List<UnitMovement> _unitMovements = new List<UnitMovement>();
        public int MaxAssignedTasksPerFrame = 50;
        public float TaskOnAotherLayerWeightCost = 2f;
        private float timer;


        public float waitTime = 0.1f; //run update 10 times per sec

        public event EventHandler OnPriorityChanged;

        public void RequestTask(UnitMovement requester)
        {
            _unitMovements.Add(requester);
        }

        public BaseTask AddTask(BaseTask baseTask)
        {
            if (!_tasksByType.ContainsKey(baseTask.Type)) _tasksByType.Add(baseTask.Type, new List<BaseTask>());
            _tasksByType[baseTask.Type].Add(baseTask);
            return baseTask;
        }

        public void RemoveTask(BaseTask baseTask)
        {
            if (!_tasksByType.ContainsKey(baseTask.Type)) return;

            if (baseTask.AssignedUnit != null) baseTask.AssignedUnit.currentTask = null;

            _tasksByType[baseTask.Type].Remove(baseTask);
        }

        public BaseTask FindTask(BaseTile tile, BaseTask.Category inCategory)
        {
            if (!_tasksByType.ContainsKey(inCategory))
                return null;

            var test = _tasksByType[inCategory].Where(x => x.TargetTile == tile);
            return test.Any() ? test.ElementAt(0) : null;
        }

        private void Awake2()
        {
            SetupTaskType(BaseTask.Category.Build, 1);
            SetupTaskType(BaseTask.Category.Mine, 2);
            SetupTaskType(BaseTask.Category.Haul, 3);
        }

        private void SetupTaskType(BaseTask.Category type, int startPrio)
        {
            _priority.Add(startPrio, type);
            _tasksByType.Add(type, new List<BaseTask>());
            _TaskEnabled.Add(type, true);
        }

        private void LateUpdate()
        {
            timer += Time.deltaTime;
            if (timer < waitTime) return;

            timer = 0f;

            if (_unitMovements.Count <= 0)
                return;
            var assignedTasks = 0;

            for (var priority = 1; priority <= _priority.Count; priority++)
            {
                var category = _priority[priority];

                if (!_TaskEnabled[category])
                    continue;

                var baseTasks = _tasksByType[category];
                var tasks = baseTasks.Where(t => t.IsAvailable()).ToList();
                if (tasks.Count == 0)
                    continue;

                while (_unitMovements.Any())
                {
                    var closestTask = GetClosestTask(ref tasks, _unitMovements.First().transform.position);

                    //There is no more tasks by this type
                    if (closestTask == null)
                        break;

                    //to spread out the work
                    if (assignedTasks++ == MaxAssignedTasksPerFrame)
                        return;

                    //get closest unit that is available
                    var closestUnit = GetClosestUnit(closestTask.TargetTile.Coordinate);

                    closestUnit.AssignTask(closestTask);
                    baseTasks.Remove(closestTask);
                    tasks.Remove(closestTask);
                    _unitMovements.Remove(closestUnit);
                }
            }
        }


        private UnitMovement GetClosestUnit(Vector3 testPos)
        {
            UnitMovement closestUnitmovement = null;
            var closestUnitmovementValue = float.MaxValue;

            foreach (var unitMovement in _unitMovements)
            {
                var distance = Vector3.Distance(unitMovement.transform.position, testPos);
                if (Math.Abs(unitMovement.transform.position.z - testPos.z) > 0.2f)
                    distance *= TaskOnAotherLayerWeightCost; //Todo: if there is 2 layers multyply by more

                if (distance < closestUnitmovementValue)
                {
                    closestUnitmovementValue = distance;
                    closestUnitmovement = unitMovement;
                }
            }

            return closestUnitmovement;
        }

        private BaseTask GetClosestTask(ref List<BaseTask> list, Vector3 inFromPos)
        {
            BaseTask closest = null;
            var closestValue = float.MaxValue;

            foreach (var task in list)
            {
                var distance = Vector3.Distance(task.TargetTile.Coordinate, inFromPos);
                if (Math.Abs(task.TargetTile.Z - inFromPos.z) > 0.2f)
                    distance *= TaskOnAotherLayerWeightCost; //Todo: if there is 2 layers multyply by more

                if (distance < closestValue)
                {
                    closestValue = distance;
                    closest = task;
                }
            }

            return closest;
        }

        public int GetPriorityValue(BaseTask.Category taskCategory)
        {
            return _priority.First(x => x.Value == taskCategory).Key;
        }

        public void ChangePrio(BaseTask.Category taskCategory, int newValue)
        {
            var oldValue = GetPriorityValue(taskCategory);
            if (oldValue == newValue) return;

            _priority[oldValue] = _priority[newValue];
            _priority[newValue] = taskCategory;

            OnPriorityChanged.Invoke(null, null);
        }

        public void StopIgnoringTaskCategory(BaseTask.Category taskCategory)
        {
            _TaskEnabled[taskCategory] = true;
        }

        public void BeginIgnoringTaskCategory(BaseTask.Category taskCategory)
        {
            _TaskEnabled[taskCategory] = false;
        }

        #region singleton

        //Singleton
        private static TaskManager _instance;

        private void Awake()
        {
            _instance = this;
            Awake2();
        }

        public static TaskManager GetInstance()
        {
            return _instance;
        }

        #endregion
    }
}