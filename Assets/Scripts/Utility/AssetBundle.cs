﻿using System;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Utility
{
    public class AssetBundle : MonoBehaviour
    {
        private static AssetBundle _instance;

        private void Awake()
        {
            _instance = this;
        }

        public static AssetBundle GetInstance()
        {
            return _instance;
        }

        #region prefabs

        [Serializable]
        public class Prefab
        {
            public string name;
            public GameObject prefabGameObject;
        }

        public Prefab[] prefabs;

        public GameObject GetPrefab(string name)
        {
            return (
                from prefab in prefabs
                where string.Equals(prefab.name, name, StringComparison.CurrentCultureIgnoreCase)
                select prefab.prefabGameObject).FirstOrDefault();
        }

        #endregion


        #region sprites

        [Serializable]
        public class Spritess
        {
            public string name;
            public Sprite SpriteObject;
        }

        public Spritess[] sprites;

        public Sprite GetSprite(string name)
        {
            foreach (var sprite in sprites)
                if (sprite.name.ToUpper() == name.ToUpper())
                    return sprite.SpriteObject;

            return null;
        }

        #endregion
    }
}