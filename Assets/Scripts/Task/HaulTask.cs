﻿using System;
using Assets.Scripts.BuildableObject;
using Assets.Scripts.Item;
using Assets.Scripts.Tile;

namespace Assets.Scripts.Task
{
    public class HaulTask : BaseTask
    {
        public HaulTask(BaseItem item) : base(Category.Haul)
        {
            TargetGameObject = item.gameObject;
            TargetItemType = item.GetComponent<BaseItem>().Type();
            TargetTile = TileManager.GetInstance().GetTileFromVector3(item.transform.position);
            CheckIfMatchingStockpileExist(null, null);

            //possible to move this to TaskManager and iterate over the list
            StockpileManager.GetInstance().OnStockpileChanges += CheckIfMatchingStockpileExist;
        }

        public bool MatchingStockpileExist { get; set; }

        public override bool IsAvailable()
        {
            return base.IsAvailable() &&
                   TileManager.GetInstance().IsTileReachable(TargetTile.Coordinate) &&
                   MatchingStockpileExist;
        }

        public override void Complete()
        {
            StockpileManager.GetInstance().OnStockpileChanges -= CheckIfMatchingStockpileExist;
        }

        public void UpdateStockpile()
        {
            if (TargetGameObject != null && TargetStockpile == null)
                TargetStockpile = StockpileManager.GetInstance().GetClosestStockpile(
                    TargetGameObject.transform.position, TargetGameObject.GetComponent<BaseItem>().Type(), true);
        }

        public void CheckIfMatchingStockpileExist(object sender, EventArgs e)
        {
            if (TargetGameObject != null && TargetGameObject.GetComponent<BaseItem>() != null)
                MatchingStockpileExist = StockpileManager.GetInstance().CheckIfStockpileExist(TargetItemType, true);
            UpdateStockpile();
        }
    }
}