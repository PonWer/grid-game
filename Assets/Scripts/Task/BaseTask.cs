﻿using Assets.Scripts.BuildableObject;
using Assets.Scripts.Item;
using Assets.Scripts.Tile;
using Assets.Scripts.Unit;
using UnityEngine;

namespace Assets.Scripts.Task
{
    public abstract class BaseTask
    {
        public enum Category
        {
            Mine = 1,
            Haul,
            Build
        }

        protected BaseTask(Category type)
        {
            Type = type;
        }

        public UnitMovement AssignedUnit { get; set; }
        public GameObject TargetGameObject { get; set; }
        public ItemType TargetItemType { get; set; }
        public BaseTile TargetTile { get; set; }
        public BaseObject TagetBaseObject { get; set; }
        public Stockpile TargetStockpile { get; set; }
        public Unbuilt TargetUnbuilt { get; set; }

        public Category Type { get; }


        public virtual bool IsAvailable()
        {
            return AssignedUnit == null;
        }

        public abstract void Complete();
    }
}