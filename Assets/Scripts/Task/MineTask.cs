﻿using System;
using Assets.Scripts.Tile;

namespace Assets.Scripts.Task
{
    public class MineTask : BaseTask
    {
        public MineTask(BaseTile targetTile1) : base(Category.Mine)
        {
            TargetTile = targetTile1;
        }

        public override void Complete()
        {
            throw new NotImplementedException();
        }

        public override bool IsAvailable()
        {
            return base.IsAvailable() && TileManager.GetInstance().IsTileReachable(TargetTile.Coordinate);
        }
    }
}