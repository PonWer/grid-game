﻿using System;
using Assets.Scripts.BuildableObject;
using Assets.Scripts.Item;
using Assets.Scripts.Tile;

namespace Assets.Scripts.Task
{
    public class BuildTask : BaseTask
    {
        public BuildTask(Unbuilt unbuilt, ItemType buildingBlock) : base(Category.Build)
        {
            TargetItemType = buildingBlock;
            TargetTile = unbuilt.ParentTile;
            TargetUnbuilt = unbuilt;

            OnStockpileChanges(null, null);

            //possible to move this to TaskManager and iterate over the list
            StockpileManager.GetInstance().OnStockpileChanges += OnStockpileChanges;
        }

        public bool MatchingStockpileExist { get; set; }
        

        public override bool IsAvailable()
        {
            return base.IsAvailable() &&
                   TileManager.GetInstance().IsTileReachable(TargetTile.Coordinate) &&
                   (TargetItemType == ItemType.Nothing || CheckIfMatchingStockpileExist());
        }

        public override void Complete()
        {
            StockpileManager.GetInstance().OnStockpileChanges -= OnStockpileChanges;
        }

        public void UpdateTargetStockpile()
        {
            if (TargetUnbuilt.RemaningCost > 0 && AssignedUnit != null)
                TargetStockpile = StockpileManager.GetInstance()
                    .GetClosestStockpile(AssignedUnit.transform.position, TargetItemType, false);
        }

        public bool CheckIfMatchingStockpileExist()
        {
            MatchingStockpileExist = StockpileManager.GetInstance().CheckIfStockpileExist(TargetItemType, false);
            return MatchingStockpileExist;
        }

        public void OnStockpileChanges(object sender, EventArgs e)
        {
            if (CheckIfMatchingStockpileExist())
                UpdateTargetStockpile();
        }
    }
}