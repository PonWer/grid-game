﻿using System;
using UnityEngine;

namespace Assets.Scripts.Item
{
    [Flags]
    public enum ItemType
    {
        Nothing = 0x000,
        Stone = 0x0001,
        Gravel = 0x0002,
        Wheat  = 0x0004,
        Everything = Nothing | Stone | Gravel | Wheat
    }

    public class BaseItem : MonoBehaviour
    {
        public virtual ItemType Type()
        {
            return ItemType.Stone;
        }

        public void Remove()
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
    }
}