﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Item;
using UnityEngine;

public class Wheat : BaseItem
{
    public override ItemType Type()
    {
        return ItemType.Wheat;
    }
}
