﻿using Assets.Scripts.Tile;
using UnityEngine;

namespace Assets.Scripts.Camera
{
    public class CameraMovement : MonoBehaviour
    {
        private UnityEngine.Camera _camera;
        public UnityEngine.Camera ChildCamera;

        private Vector3 _startMousePos;


        private bool _leftCtrlPressed;
        public float Max = 30f;
        public float Min = 10f;

        public float ScrollSpeed = 1f;

        private void Start()
        {
            _camera = GetComponent<UnityEngine.Camera>();
            if (ChildCamera == null)
                ChildCamera = GetComponentInChildren<UnityEngine.Camera>();

            Debug.Assert(ChildCamera != null, "child camera not found");
            Debug.Assert(ChildCamera != _camera, "child camera is camera");
        }

        private void LateUpdate()
        {
            if (Input.GetKeyDown(KeyCode.LeftControl))
                _leftCtrlPressed = true;
            if (Input.GetKeyUp(KeyCode.LeftControl))
                _leftCtrlPressed = false;


            ScrollWheel();

            ArrowsAndWASD();

            RightClick();
        }


        private void ArrowsAndWASD()
        {
            var xAxisValue = Input.GetAxis("Horizontal");
            var yAxisValue = Input.GetAxis("Vertical");

            MoveCamera(new Vector3(transform.position.x + xAxisValue,
                transform.position.y + yAxisValue,
                transform.position.z));
        }

        private void ScrollWheel()
        {
            var d = Input.GetAxis("Mouse ScrollWheel");
            if (!_leftCtrlPressed)
            {
                //Camera view size
                if (d > 0f)
                {
                    // scroll up
                    var cameraOrthographicSize = _camera.orthographicSize - ScrollSpeed;
                    _camera.orthographicSize = cameraOrthographicSize < Min ? Min : cameraOrthographicSize;
                    ChildCamera.orthographicSize = cameraOrthographicSize < Min ? Min : cameraOrthographicSize;
                }
                else if (d < 0f)
                {
                    // scroll down
                    var cameraOrthographicSize = _camera.orthographicSize + ScrollSpeed;
                    _camera.orthographicSize = cameraOrthographicSize > Max ? Max : cameraOrthographicSize;
                    ChildCamera.orthographicSize = cameraOrthographicSize > Max ? Max : cameraOrthographicSize;
                }
            }
            else
            {
                //Layer Movement
                if (d > 0f)
                {
                    if (GameManager.GetInstance().CurrentWorldLayer + 1 < TileManager.GetInstance().maxZ)
                    {
                        GameManager.GetInstance().CurrentWorldLayer++;
                        UnityEngine.Camera.main.transform.position += new Vector3(0, 0, 1);
                    }
                }
                else if (d < 0f)
                {
                    if (GameManager.GetInstance().CurrentWorldLayer > 0)
                    {
                        GameManager.GetInstance().CurrentWorldLayer--;
                        UnityEngine.Camera.main.transform.position -= new Vector3(0, 0, 1);
                    }
                }
            }
        }

        private void RightClick()
        {
            if (Input.GetMouseButtonDown(1))
            {
                _startMousePos = _camera.ScreenToWorldPoint(Input.mousePosition);
                _startMousePos.z = 0.0f;
            }

            if (Input.GetMouseButton(1))
            {
                var nowMousePos = _camera.ScreenToWorldPoint(Input.mousePosition);
                nowMousePos.z = 0.0f;
                MoveCamera(transform.position + _startMousePos - nowMousePos);
            }
        }

        private void MoveCamera(Vector3 pos)
        {
            var maxX = TileManager.GetInstance().maxX - _camera.orthographicSize * _camera.aspect;
            var maxY = TileManager.GetInstance().maxY - _camera.orthographicSize;

            pos = new Vector3(
                Mathf.Clamp(pos.x, _camera.orthographicSize * _camera.aspect, maxX),
                Mathf.Clamp(pos.y, _camera.orthographicSize, maxY),
                pos.z);

            transform.position = pos;
        }
    }
}