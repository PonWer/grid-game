﻿using System;
using System.Collections.Generic;
using Assets.Scripts.BuildableObject;
using Assets.Scripts.Tile;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Camera
{
    public class CameraSelector : MonoBehaviour
    {
        protected internal GameManager GameManager;

        protected internal Vector3 MouseDownPoint;
        protected internal Vector3Int MouseDownPointInt;
        private bool _mouseIsDown;
        public GUIStyle MouseSkin;
        protected internal TileManager TileManager;

        protected GameManager.State CurrentState => GameManager.GetState();


        // Update is called once per frame
        private void LateUpdate()
        {
            if (EventSystem.current.IsPointerOverGameObject()) //IsMouseOverUIElement
                return;

            if (Input.GetKey("escape")) Application.Quit();

            if (Input.GetMouseButtonDown(0))
            {
                MouseDownPoint = GetPosUsingMousePos();
                MouseDownPointInt = GetPosUsingMousePosInt();
                _mouseIsDown = true;
            }

            if (Input.GetMouseButton(0))
            {
            }

            if (Input.GetMouseButtonUp(0))
            {
                _mouseIsDown = false;

                var mouseUpPoint = GetPosUsingMousePosInt();
                var targeTile = TileManager.GetTileFromVector3(mouseUpPoint);


                switch (CurrentState)
                {
                    case GameManager.State.Default:
                        break;
                    case GameManager.State.Mine:
                        var mode = !TileManager.GetTileFromVector3(MouseDownPointInt).AssignedToATask;

                        foreach (var baseTile in GetTileList(MouseDownPointInt, mouseUpPoint))
                            baseTile.Selected_Mined(mode);
                        break;
                    case GameManager.State.Move:
                        break;
                    case GameManager.State.Stockpile:
                        if (targeTile == null || !targeTile.Walkable || targeTile.OccupiedBy != null)
                            return;

                        TileManager.GetInstance().BuildUnbuilt(BaseObject.ObjectType.Stockpile, targeTile);
                        break;
                    case GameManager.State.Stairs:
                        var targeTilebelow = TileManager.GetTileFromVector3(mouseUpPoint + new Vector3Int(0, 0, 1));
                        if (targeTile == null || !targeTile.Walkable || targeTile.OccupiedBy != null)
                            return;
                        //if (targeTilebelow == null || (targeTilebelow.Mineable || targeTilebelow.Walkable))
                        //    return;


                        //on level
                        var stairsOnLevel = TileManager.GetInstance()
                            .BuildUnbuilt(BaseObject.ObjectType.Stairs, targeTile);
                        stairsOnLevel.tileBelow = targeTilebelow;
                        break;
                    case GameManager.State.Wall:
                        foreach (var baseTile in GetTileList(MouseDownPointInt, mouseUpPoint))
                            if (baseTile.OccupiedBy == null && baseTile.Walkable)
                                TileManager.BuildUnbuilt(BaseObject.ObjectType.Wall, baseTile);
                        break;
                    case GameManager.State.Soil:
                        foreach (var baseTile in GetTileList(MouseDownPointInt, mouseUpPoint))
                            if (baseTile.OccupiedBy == null && baseTile.Walkable)
                                TileManager.BuildUnbuilt(BaseObject.ObjectType.Soil, baseTile);
                        break;

                }
            }
        }

        private void OnGUI()
        {
            if (!_mouseIsDown)
                return;

            switch (CurrentState)
            {
                case GameManager.State.Default:
                    return;
                case GameManager.State.Mine:
                    break;
                case GameManager.State.Move:
                    break;
                case GameManager.State.Stockpile:
                    return;
                case GameManager.State.Stairs:
                    return;
                case GameManager.State.Wall:
                    break;
            }

            var startScreenPos = UnityEngine.Camera.main.WorldToScreenPoint(MouseDownPointInt);
            var endScreenPos = UnityEngine.Camera.main.WorldToScreenPoint(GetPosUsingMousePosInt());

            var boxWidth = startScreenPos.x - endScreenPos.x;
            var boxHeight = startScreenPos.y - endScreenPos.y;

            var boxLeft = endScreenPos.x;
            var boxtop = Screen.height - endScreenPos.y - boxHeight;

            UnityEngine.GUI.Box(new Rect(boxLeft, boxtop, boxWidth, boxHeight), "", MouseSkin);
        }

        private Vector3Int GetPosUsingMousePosInt()
        {
            var pos = UnityEngine.Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var posInt = new Vector3Int(Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.y),
                GameManager.CurrentWorldLayer);
            return posInt;
        }

        private Vector3 GetPosUsingMousePos()
        {
            var pos = UnityEngine.Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos.Set(pos.x, pos.y, GameManager.CurrentWorldLayer);
            return pos;
        }

        private List<BaseTile> GetTileList(Vector3Int start, Vector3Int end)
        {
            var startX = Math.Min(start.x, end.x);
            var endX = Math.Max(start.x, end.x);

            var startY = Math.Min(start.y, end.y);
            var endY = Math.Max(start.y, end.y);

            var returnList = new List<BaseTile>();

            for (var x = startX; x <= endX; x++)
            for (var y = startY; y <= endY; y++)
                returnList.Add(TileManager.GetTile(new Vector3Int(x, y, GameManager.CurrentWorldLayer)));

            return returnList;
        }

        private void Start()
        {
            TileManager = TileManager.GetInstance();
            GameManager = GameManager.GetInstance();
        }
    }
}